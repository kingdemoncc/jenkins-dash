FROM node:alpine

WORKDIR /usr/src/app/

COPY package.json /usr/src/app/
COPY node_modules /usr/src/app/node_modules
COPY public /usr/src/app/public
COPY src /usr/src/app/src


RUN "ls" "/usr/src/app/"
EXPOSE 3000
ENTRYPOINT [ "node", "./src/index.js"]