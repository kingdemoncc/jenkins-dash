const jenkinsApiService = require('../src/service/jenkinsApiService');

var assert = require('assert');
describe('request', function () {
    describe('request-jenkins-api', function () {
        it('request http://192.168.11.219:8000/api/json', function () {
            let jenkinsService = new jenkinsApiService();
            jenkinsService.AllProjectStatus((data) => {
                console.log(data);
                JSON.parse(data)
            });
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });
});