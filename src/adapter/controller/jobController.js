var express = require('express')
var router = express.Router()
const jenkinsApiService = require("../../service/jenkinsApiService");

router.get("/", function (req, res) {
    let jenkinsService = new jenkinsApiService();
    jenkinsService.AllProjectStatus((data) => res.send(data));
});

module.exports = router;