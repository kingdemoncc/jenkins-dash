const express = require('express')
const app = express()
const port = 3000

//app.get("/", (rep, res) => res.send("hi go to <a href='jobs'>jobs</a>"));

const jobController = require("./adapter/controller/jobController");
app.use('/jobs', jobController);

app.use(express.static('public'))
//app.listen(port, "0.0.0.0", () => console.log(`Example app listening on port ${port}!`))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))